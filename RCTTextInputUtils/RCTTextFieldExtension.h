//
//  RCTTextFieldExtension.h
//  RCTKeyboardToolbar
//
//  Created by Kanzaki Mirai on 11/10/15.
//  Copyright © 2015 DickyT. All rights reserved.
//

#import "RCTTextField.h"
#import "RCTTextInput.h"

@interface RCTTextField (RCTTextFieldExtension)

- (void)setSelectedRange:(NSRange)range;
- (void)invalidateInputAccessoryView;
@end
